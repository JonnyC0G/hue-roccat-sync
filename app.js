const {hexToRgb, rgbToHex} = require('./src/helpers.js')
const roccatDevices = require("roccat-led");
const {pollScreen} = require("./src/screen/screen-api");
const {subscribeToHueEvents, getCurrentLightstripColorAndSync} = require("./src/hue/hue-api");

//time in ms between requests
const interval = 300;

let mouse = new roccatDevices.RoccatKone({
        ready: () => {
            lightingMethods.push((hexValue) => {
                mouse.fillAll(hexValue);
                mouse.render();
            })
            isReady();
        }
    }
);

let mousepad = new roccatDevices.RoccatSense({
        ready: () => {
            lightingMethods.push((hexValue) => {
                mousepad.fillAll(hexValue);
                mousepad.render();
            })
            isReady();
        }
    }
);

let lastColor = '#ffffff'
let keyboard = new roccatDevices.RoccatVulcan({
        layout: 'de-de',
        ready: () => {
            lightingMethods.push((hexValue) => {
                //keyboard.fillAll(hexValue);
                animateKeyboard(keyboard, lastColor, hexValue, interval)
                //keyboard.render();
                lastColor = hexValue;
            })
            isReady();
        }
    }
)

//this should be in the module
function animateKeyboard(keyboard, colorFrom, colorTo, duration) {
    const start = Date.now();
    let rgbFrom = hexToRgb(colorFrom);
    let rgbTo = hexToRgb(colorTo);
    let rgbRunning = Object.assign({}, rgbFrom);

    const rMax = rgbTo.r - rgbFrom.r;
    const gMax = rgbTo.g - rgbFrom.g;
    const bMax = rgbTo.b - rgbFrom.b;

    const timer = setInterval(() => {

        let runningTime = Date.now() - start;
        runningTime = runningTime > duration ? duration : runningTime;

        //Calculate new RGB-Value
        const percentage = 100 / duration * runningTime;
        rgbRunning.r = Math.round(rgbFrom.r + rMax / 100 * percentage);
        rgbRunning.g = Math.round(rgbFrom.g + gMax / 100 * percentage);
        rgbRunning.b = Math.round(rgbFrom.b + bMax / 100 * percentage);

        //Send new Value
        keyboard.fillAll(rgbToHex(rgbRunning.r, rgbRunning.g, rgbRunning.b));
        keyboard.render();

        //Clear Timer if duration ends
        if (runningTime >= duration) {
            const t = keyboard.animateTimers.find(e => e === timer)
            if (t)
                clearInterval(t)
        }

    }, 10);
    keyboard.animateTimers.push(timer);
}


let lightingMethods = [];

function isReady() {
    if (lightingMethods.length !== 3) {
        console.log('waiting until all devices are initialized')
    } else {
        // Syncing to HUE
        //subscribeToHueEvents(getCurrentLightstripColorAndSync, lightingMethods)

        // Syncing to screen
        pollScreen(interval, lightingMethods)
        console.log('syncing started');
    }
}


