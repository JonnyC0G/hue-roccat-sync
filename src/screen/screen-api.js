const fs = require("fs");
const {performance} = require("perf_hooks");
const {exec} = require("child_process");
const sharp = require("sharp");
const helpers = require("../helpers.js");
const {sleep} = require("roccat-led/src/helpers");
const ColorThief = require("colorthief")

module.exports.pollScreen = function pollScreen(interval, lightCallback) {
    cleanUp();
    //start polling
    setInterval(() => getScreenColor(lightCallback), interval);
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

function cleanUp() {
    for (let j = 0; j <= 99999; j++) {
        //clear the mess up
        fs.unlink('./temp/screen' + j + '.jpg', (err) => {
        });
        fs.unlink('./temp/croppedScreen' + j + '.jpg', (err) => {
        });
    }
}

function getScreenColor(lightCallback) {
    let rdm = getRandomInt(99999);

    // https://superuser.com/questions/75614/take-a-screen-shot-from-command-line-in-windows
    let startTime2 = performance.now();
    exec('call "./src/screen/screenCapture" ./temp/screen' + rdm + '.jpg', function (error, stdout, stderr) {
        let endTime2 = performance.now();
        if (error) throw error;

        console.log('screenshot taken > ./temp/screen' + rdm + '.jpg', endTime2 - startTime2);

        let startTime3 = performance.now();
        // https://usefulangle.com/post/104/nodejs-crop-image
        sharp('./temp/screen' + rdm + '.jpg').extract({
            width: 3840,
            height: 750,
            left: 0,
            top: 1410
        }).toFile('./temp/croppedScreen' + rdm + '.jpg')
            .then(function (new_file_info) {
                let endTime3 = performance.now();
                console.log('screenshot cropped > ./temp/croppedScreen' + rdm + '.jpg', endTime3 - startTime3);

                let startTime4 = performance.now();
                // https://lokeshdhakar.com/projects/color-thief/
                ColorThief.getColor('./temp/croppedScreen' + rdm + '.jpg', 100)
                    .then(color => {
                        let endTime4 = performance.now();
                        console.log('color extracted to rgb >', color, endTime4 - startTime4)
                        let hexVal = helpers.rgbToHex(color[0], color[1], color[2]);
                        console.log('color converted to hex >', hexVal);

                        //send commands to devices
                        for (let i = 0; i < lightCallback.length; i++) {
                            lightCallback[i](hexVal);
                        }

                        console.log('--')

                        sleep(1000).then(() => {
                            //clear the mess up (delete the last)
                            fs.unlink('./temp/screen' + (rdm) + '.jpg', (err) => {
                                if (err) {
                                    cleanUp();
                                    throw err
                                }
                                ;
                            });
                            fs.unlink('./temp/croppedScreen' + (rdm) + '.jpg', (err) => {
                                if (err) {
                                    cleanUp();
                                    throw err
                                }
                                ;
                            });
                        });
                    })
                    .catch(err => {
                        console.log(err)
                    })
            })
            .catch(function (err) {
                console.error("error cropping screenshot:", err);
                cleanUp();
                throw err;
            });
    });
}