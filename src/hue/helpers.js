const {rgbToHex} = require("../helpers");
module.exports.extractRgb = extractRgb;

function extractRgb(response) {

    //unfortunately only reads one color
    let x = response.data[0].color.xy.x;
    let y = response.data[0].color.xy.y;
    let brightness = response.data[0].dimming.brightness;

    /*
    console.log('left:', x);
    console.log('right:', y);
    console.log('brightness:', brightness);
     */

    let rgb = xyBriToRgb(x, y, brightness);

    let r2 = Math.round(rgb.r);
    let g2 = Math.round(rgb.g);
    let b2 = Math.round(rgb.b);

    /*
    console.log('rgb: ' + r2 + ' ' + g2 + ' ' + b2);
    console.log('hex: ' + hexValue);

     */

    return rgbToHex(r2, g2, b2);
}



module.exports.xyBriToRgb = xyBriToRgb;

function xyBriToRgb(x, y, bri) {
    // https://stackoverflow.com/questions/22894498/philips-hue-convert-xy-from-api-to-hex-or-rgb
    let z = 1.0 - x - y;
    let Y = bri / 255.0; // Brightness of lamp
    let X = (Y / y) * x;
    let Z = (Y / y) * z;
    let r = X * 1.612 - Y * 0.203 - Z * 0.302;
    let g = -X * 0.509 + Y * 1.412 + Z * 0.066;
    let b = X * 0.026 - Y * 0.072 + Z * 0.962;
    r = r <= 0.0031308 ? 12.92 * r : (1.0 + 0.055) * Math.pow(r, (1.0 / 2.4)) - 0.055;
    g = g <= 0.0031308 ? 12.92 * g : (1.0 + 0.055) * Math.pow(g, (1.0 / 2.4)) - 0.055;
    b = b <= 0.0031308 ? 12.92 * b : (1.0 + 0.055) * Math.pow(b, (1.0 / 2.4)) - 0.055;
    let maxValue = Math.max(r, g, b);
    r /= maxValue;
    g /= maxValue;
    b /= maxValue;
    r = r * 255;
    if (r < 0) {
        r = 255
    }

    g = g * 255;
    if (g < 0) {
        g = 255
    }

    b = b * 255;
    if (b < 0) {
        b = 255
    }

    return {
        r: r,
        g: g,
        b: b
    }
}

module.exports.bytesToString = function (array) {
    let result = "";
    for (let i = 0; i < array.length; i++) {
        result += String.fromCharCode(parseInt(array[i], 16));
    }
    return result;
}