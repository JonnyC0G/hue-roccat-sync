const consts = require("./consts");
const https = require("https");
const helpers = require("../helpers.js");

module.exports = function getCurrentLightstripColorAndSync(lightCallback) {
    const options = {
        hostname: consts.BRIDGE_IP,
        path: '/clip/v2/resource/light/' + consts.LIGHTSTRIP_ID,
        headers: {
            'hue-application-key': consts.CLIENT_KEY
        },
        rejectUnauthorized: false,
    }
    let requestStream = https.request(options, (response) => {
        if (response.statusCode < 200 || response.statusCode >= 300) {
            console.error(new Error('statusCode=' + response.statusCode));
        }

        let body = [];
        response.on('data', function (chunk) {
            body.push(chunk);
            //javascript has to build the response first
        });
        response.on('end', function () {
            try {
                body = JSON.parse(Buffer.concat(body).toString());
            } catch (e) {
                console.error(e);
            }
            let hexValue = helpers.extractRgb(body);

            //send the extracted Value to all devices
            for (let i = 0; i < lightCallback.length; i++) {
                lightCallback[i](hexValue);
            }
        });
    });
    requestStream.end();
}

module.exports = function subscribeToHueEvents(pollMethod, lightCallback) {
    const options = {
        hostname: consts.BRIDGE_IP,
        path: '/eventstream/clip/v2',
        headers: {
            'hue-application-key': consts.CLIENT_KEY,
            'Accept': 'text/event-stream',
        },
        rejectUnauthorized: false,
    }

    let request = https.request(options, (response) => {

        response.on('data', (d) => {
            let content = d.toString('binary');
            if (content.indexOf("hi") > -1) {
                return;
            }

            //change event happened -> poll
            pollMethod(lightCallback);
        });
    });

    request.on('error', (e) => {
        console.error('error thrown')
        console.error(e)
    })

    request.end();
}